import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.lang.IgniteRunnable;
import org.apache.ignite.spi.deployment.DeploymentSpi;
import org.apache.ignite.spi.deployment.uri.UriDeploymentSpi;

import java.util.Arrays;

/**
 * Copyright Prorabel BVBA
 * <p/>
 * User: sbeaupre
 * Date: 08/01/16
 * Time: 13:18
 *
 *
 *    -DIGNITE_SKIP_CONFIGURATION_CONSISTENCY_CHECK=true
 *
 *
 */
public class _03GarTest {

    public static void main(String[] args) {

        System.out.println("Start urideployment test");

        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setPeerClassLoadingEnabled(true); //needs to be the same as in the XML for the server
        cfg.setClientMode(true);

        UriDeploymentSpi deploymentSpi = new UriDeploymentSpi();

        deploymentSpi.setUriList(Arrays.asList("file:///Users/sbeaupre/Dropbox/prorabel/Projects/IgniteTests/ignite/gar"));

        cfg.setDeploymentSpi(deploymentSpi);

        try(Ignite ignite = Ignition.start(cfg)) {
            IgniteCluster grp = ignite.cluster();
//            ignite.compute(grp).broadcast(
//                    new IgniteRunnable() {
//                        @Override public void run() {
//                            // Print ID of remote node on remote node.
//                            System.out.println(">>> Hello Node: " + grp.ignite().cluster().localNode().id());
//                            try {
//                                System.out.println(Class.forName("SimpleTask"));
//                            } catch (ClassNotFoundException e) {
//                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                            }
//                        }
//                    }
//            );

            ignite.compute(ignite.cluster().forServers()).execute("myproject.HelloWorldTask", "this is a test");
        }

    }
}
