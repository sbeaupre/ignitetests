package myproject;

import org.apache.ignite.IgniteException;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.compute.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright Prorabel BVBA
 * <p/>
 * User: sbeaupre
 * Date: 11/01/16
 * Time: 16:48
 */
@ComputeTaskName("SimpleTaskName")
public class SimpleTask implements ComputeTask<String, Integer> {


    static {
        System.out.println("Loading SimpleTask");
    }

    public SimpleTask() {
    }

    @Override
    public Map<? extends ComputeJob, ClusterNode> map(List<ClusterNode> subgrid, String arg) throws IgniteException {
        System.out.println("Computing Job in SimpleTask ");
        return new HashMap<>();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ComputeJobResultPolicy result(ComputeJobResult res, List<ComputeJobResult> rcvd) throws IgniteException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Integer reduce(List<ComputeJobResult> results) throws IgniteException {
        return Integer.valueOf(0);  //To change body of implemented methods use File | Settings | File Templates.
    }
}
