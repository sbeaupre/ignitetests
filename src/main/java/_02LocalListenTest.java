import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;
import org.apache.ignite.events.TaskEvent;
import org.apache.ignite.lang.IgnitePredicate;
import org.apache.ignite.lang.IgniteRunnable;

import static org.apache.ignite.events.EventType.EVTS_TASK_EXECUTION;

/**
 * Copyright Prorabel BVBA
 * <p/>
 * User: sbeaupre
 * Date: 07/01/16
 * Time: 21:48
 */
public class _02LocalListenTest {

    /**
     * Executes example.
     *
     * @param args Command line arguments, none required.
     * @throws Exception If example execution failed.
     */
    public static void main(String[] args) throws Exception {
        try (Ignite ignite = Ignition.start("./ignite-config.xml")) {
            System.out.println();
            System.out.println(">>> Events API example started.");

            // Listen to events happening on local node.
            localListen();


            // Wait for a while while callback is notified about remaining puts.
            Thread.sleep(1000);
        }
    }

    /**
     * Listen to events that happen only on local node.
     *
     * @throws org.apache.ignite.IgniteException
     *          If failed.
     */
    private static void localListen() throws IgniteException {
        System.out.println();
        System.out.println(">>> Local event listener example.");

        Ignite ignite = Ignition.ignite();

        ignite.compute().broadcast(
                new IgniteRunnable() {
                    @Override
                    public void run() {
                        Ignite ignite = Ignition.ignite();

                        // Print ID of remote node on remote node.
                        System.out.println(">>> Hello Node: " + ignite.cluster().localNode().id());

                        IgnitePredicate<TaskEvent> lsnr = new IgnitePredicate<TaskEvent>() {
                            @Override
                            public boolean apply(TaskEvent evt) {
                                System.out.println("Received task event [evt=" + evt.name() + ", taskName=" + evt.taskName() + ']');

                                return true; // Return true to continue listening.
                            }
                        };


                        // Register event listener for all local task execution events.
                        ignite.events().localListen(lsnr, EVTS_TASK_EXECUTION);


                        // Unsubscribe local task event listener.
                        //ignite.events().stopLocalListen(lsnr);
                    }
                }
        );


    }


}
