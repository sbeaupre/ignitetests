import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.compute.ComputeTaskSession;
import org.apache.ignite.lang.IgniteRunnable;
import org.apache.ignite.resources.TaskSessionResource;

import static org.apache.ignite.events.EventType.EVTS_TASK_EXECUTION;

/**
 * Copyright Prorabel BVBA
 * <p/>
 * User: sbeaupre
 * Date: 07/01/16
 * Time: 21:48
 */
public class _02TaskLauncherTest {

    /**
     * Executes example.
     *
     * @param args Command line arguments, none required.
     * @throws Exception If example execution failed.
     */
    public static void main(String[] args) throws Exception {
        try (Ignite ignite = Ignition.start("./ignite-config.xml")) {
            System.out.println();
            System.out.println(">>> Events API example started.");

            // Generate task events.
            for (int i = 0; i < 10; i++) {
                ignite.compute().withName(i < 5 ? "good-task-" + i : "bad-task-" + i).run(new IgniteRunnable() {
                    // Auto-inject task session.
                    @TaskSessionResource
                    private ComputeTaskSession ses;

                    @Override public void run() {
                        System.out.println("Executing sample job for task: " + ses.getTaskName());
                    }
                });
            }




            // Wait for a while while callback is notified about remaining puts.
            Thread.sleep(1000);
        }
    }




}
