import examples.ExamplesUtils;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.lang.IgnitePredicate;
import org.apache.ignite.lang.IgniteRunnable;

/**
 * Copyright Prorabel BVBA
 * <p/>
 * User: sbeaupre
 * Date: 07/01/16
 * Time: 20:27
 */
public class EventTest {


    public static void main(String[] args) throws IgniteException {
        try (Ignite ignite = Ignition.start("./ignite-config.xml")) {
            if (!ExamplesUtils.checkMinTopologySize(ignite.cluster(), 2))
                return;

            System.out.println();
            System.out.println("Compute example started.");

            IgniteCluster cluster = ignite.cluster();

            // Say hello to all nodes in the cluster, including local node.
            sayHello(ignite, cluster);

            // Say hello to all remote nodes.
            sayHello(ignite, cluster.forRemotes());

            // Pick random node out of remote nodes.
            ClusterGroup randomNode = cluster.forRemotes().forRandom();

            // Say hello to a random node.
            sayHello(ignite, randomNode);

            // Say hello to all nodes residing on the same host with random node.
            sayHello(ignite, cluster.forHost(randomNode.node()));

            // Say hello to all nodes that have current CPU load less than 50%.
            sayHello(ignite, cluster.forPredicate(new IgnitePredicate<ClusterNode>() {
                @Override public boolean apply(ClusterNode n) {
                    return n.metrics().getCurrentCpuLoad() < 0.5;
                }
            }));
        }
    }

    /**
     * Print 'Hello' message on remote nodes.
     *
     * @param ignite Ignite.
     * @param grp Cluster group.
     * @throws IgniteException If failed.
     */
    private static void sayHello(Ignite ignite, final ClusterGroup grp) throws IgniteException {
        // Print out hello message on all cluster nodes.
        ignite.compute(grp).broadcast(
                new IgniteRunnable() {
                    @Override public void run() {
                        // Print ID of remote node on remote node.
                        System.out.println(">>> Hello Node: " + grp.ignite().cluster().localNode().id());
                    }
                }
        );
    }
}
